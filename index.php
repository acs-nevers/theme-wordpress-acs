<?php

get_header(); ?>

	<div id="primary">
    <?php get_sidebar(); ?>

		<main id="main">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

      <?php get_template_part( 'content', get_post_format() ); ?>

    <?php endwhile; endif; ?>

		</main>
	</div>

<?php get_footer(); ?>
