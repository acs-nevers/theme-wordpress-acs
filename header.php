<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/style.css">
	<link rel="icon" href="<?php echo get_bloginfo('template_directory'); ?>/img/favicon.png">
	<title><?php echo get_bloginfo( 'name' ); ?></title>
	<?php wp_head();?>
</head>
<body>

<header id="main-header">
	<div class="bg-code">
		<a href="<?php echo get_bloginfo( 'wpurl' );?>">
			<img id="nbc" src="<?php echo get_bloginfo('template_directory'); ?>/img/logo.png">
		</a>
	</div>
</header>
