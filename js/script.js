var burger = document.getElementById('burger');
var bMenu = document.getElementById('burger-menu');

burger.onclick = function() {
  if (burger.classList.contains('active')) {
    closeBurger();
  } else {
    openBurger();
  }
}

function openBurger() {
  burger.classList.add('active');
  bMenu.classList.add('visible');
}

function closeBurger() {
  burger.classList.remove('active');
  bMenu.classList.remove('visible');
}

window.onresize = closeBurger;
