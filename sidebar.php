<aside id="sidebar">
  <!-- Search -->
  <?php get_search_form() ?>

  <div id="burger">
    <div class="burger-bar"></div>
    <div class="burger-bar"></div>
    <div class="burger-bar"></div>
  </div>

  <div id="other">
    <!-- Recent posts -->
    <h4>Articles récents</h4>
    <ol>
      <?php
      $args = array(
        'numberposts' => 5,
        'orderby' => 'post_date',
        'order' => 'DESC'
      );
      $recent_posts = wp_get_recent_posts($args);
      foreach( $recent_posts as $recent ){
        echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';
      }
      ?>
    </ol>

    <!-- Archives -->
    <h4>Archives</h4>
    <ol>
      <?php wp_get_archives( 'type=monthly' ); ?>
    </ol>

    <!-- Categories -->
    <h4>Catégories</h4>
    <ul>
      <?php
      $args = array(
        // 'echo' => false,
        'exclude' => '1',
        'number' => 5,
        'title_li' => ''
      );
      wp_list_categories($args);
      ?>
    </ul>

    <img src="<?php echo get_bloginfo('template_directory'); ?>/img/acs.png" class="acs-logo">
  </div>

</aside>

<div class="wrapper">
  <nav id="burger-menu">
    <div class="container">
      <!-- Recent posts -->
      <h4>Articles récents</h4>
      <ol>
        <?php
        $args = array(
          'numberposts' => 5,
          'orderby' => 'post_date',
          'order' => 'DESC'
        );
        $recent_posts = wp_get_recent_posts($args);
        foreach( $recent_posts as $recent ){
          echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';
        }
        ?>
      </ol>

      <!-- Archives -->
      <h4>Archives</h4>
      <ol>
        <?php wp_get_archives( 'type=monthly' ); ?>
      </ol>

      <!-- Categories -->
      <h4>Catégories</h4>
      <ul>
        <?php
        $args = array(
        // 'echo' => false,
          'exclude' => '1',
          'number' => 5,
          'title_li' => ''
        );
        wp_list_categories($args);
        ?>
      </ul>
    </div>
  </nav>
</div>
